/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class TestOX {
    
    public TestOX() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    /*@Test
    public void TestWinRow1_X_true(){
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char Player = 'X';
        boolean result = LabXO.statusrow(Player);
        assertEquals(true, result);
    }*/
    
    @Test
    public void testStatusRow_1_Winner() {
        char[][] testTable = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        LabXO.table = testTable;
        LabXO.row = 1;
        LabXO.Player = 'X';
        boolean result = LabXO.statusrow();
        assertEquals(true,result);
    }
    @Test
    public void testStatusRow_2_Winner() {
        char[][] testTable = {{'O', 'O', 'O'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        LabXO.table = testTable;
        LabXO.row = 2;
        LabXO.Player = 'X';
        boolean result = LabXO.statusrow();
        assertEquals(true,result);
    }
    @Test
    public void testStatusRow_3_Winner() {
        char[][] testTable = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        LabXO.table = testTable;
        LabXO.row = 3;
        LabXO.Player = 'X';
        boolean result = LabXO.statusrow();
        assertEquals(true,result);
    }
    
    @Test
    public void testStatusCol_1_Winner() {
        char[][] testTable = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        LabXO.table = testTable;
        LabXO.col = 1;
        LabXO.Player = 'X';
        boolean result = LabXO.statuscol();
        assertEquals(true,result);
    }
    @Test
    public void testStatusCol_2_Winner() {
        char[][] testTable = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        LabXO.table = testTable;
        LabXO.col = 2;
        LabXO.Player = 'X';
        boolean result = LabXO.statuscol();
        assertEquals(true,result);
    }
    @Test
    public void testStatusCol_3_Winner() {
        char[][] testTable = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        LabXO.table = testTable;
        LabXO.col = 3;
        LabXO.Player = 'X';
        boolean result = LabXO.statuscol();
        assertEquals(true,result);
    }
    
    @Test
    public void testStatusDeaws_1_Winner() {
        char[][] testTable = {{'X', 'O', 'X'}, {'O', 'X', 'O'}, {'O', 'X', 'O'}};
        LabXO.table = testTable;
        LabXO.col = 1;
        LabXO.Player = 'X';
        boolean result = LabXO.statusdraws();
        assertEquals(true,result);
    }
    
    @Test
    public void testStatusdiagonal_1_Winner() {
        char[][] testTable = {{'O', 'X', 'X'}, {'X', 'X', 'O'}, {'X', 'O', 'O'}};
        LabXO.table = testTable;
        LabXO.col = 1;
        LabXO.Player = 'X';
        boolean result = LabXO.diagonal();
        assertEquals(true,result);
    }
    @Test
    public void testStatusdiagonal_2_Winner() {
        char[][] testTable = {{'X', 'O', 'X'}, {'X', 'X', 'O'}, {'O', 'O', 'X'}};
        LabXO.table = testTable;
        LabXO.col = 1;
        LabXO.Player = 'X';
        boolean result = LabXO.diagonal();
        assertEquals(true,result);
    }

}