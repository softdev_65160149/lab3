/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author informatics
 */
import java.util.Scanner;
public class LabXO {
    
    static void Welcome(){
        System.out.println("Welcome to XO");
    }
    
    static char[][]
    table ={{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    
    static void printtable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    static char again='R';
    
    static void  resetGame(){
        if ( again=='y'){
            again = 'R';
        }
        for(int row = 0; row <3; row++){
            for(int col = 0; col<3; col++){
                table[row][col] ='-';
            }
        }
    }
    
    static char Player = 'X';
    static void turn(){
        System.out.println(Player+" turn");
    }
    
    static int row = 0;
    static int col = 0;
    static void Input(){
        Scanner input = new Scanner(System.in);
        while(true){
            System.out.print("Please input row,col : ");
            row = input.nextInt();
            col = input.nextInt();
            if (table[row-1][col-1]!='-'){
                System.out.println("Invalid move, Please input again");
                continue;
            }else{
                table [row-1][col-1]=Player;
            }
        
        break;
            
        }
    }
    
    static void check(){
        
        Scanner input = new Scanner(System.in);
        if (statusrow()  || statuscol() || diagonal() ){
            printtable();
            System.out.println("Player "+Player+" is a Winner!!");
            System.out.print("Continue? (y/n):");
            again = input.next().charAt(0);
            
        }else if (statusdraws()){
            printtable();
            System.out.println("Game Draws!!");
            System.out.print("Continue? (y/n):");
            again = input.next().charAt(0);
            
            
        }
        
    }
    
    
    
    static boolean statusrow(){
        for (int i=0;i<3;i++){
            if (table [row-1][i]!=Player){
                return false;
            }
        }
        return true;
    }
    static boolean statuscol(){
        for (int j=0;j<3;j++){
            if (table [j][col-1]!=Player){
                return false;
            }
        }
        return true;
    }
    static boolean statusdraws(){
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                if (table [i][j]=='-'){
                    return false;
                }
            }
        }
        return true;
    }
    static boolean diagonal(){
        if(table[0][0] == Player && table[1][1] == Player && table[2][2] == Player){
            return true;
        }
        if(table[2][0] == Player && table[1][1] == Player && table[0][2] == Player){
            return true;
        }
        return false;
    }
    
    static void switchplayer(){
        if(Player =='X'){
            Player='O';
        }
        else{
            Player='X';
        }
    }   
    
    
    public static void main(String[] args) {
        Welcome();
        while (true) {            
            printtable();
            turn();
            Input();
            check();
            switchplayer();
            if (again == 'y'){
                resetGame();
            }else if(again == 'n'){
                System.out.println("~~ Good Bye ~~");
                break;
            }
            
        }
    }
}